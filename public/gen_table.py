from PIL import Image
import json, sys
import numpy as np

img_html = '<img src="files/dataset/faces/{}" style="width: 100px; height: 100px;">'
ul_html = '<ul style="text-align: left;">{}\n</ul>'
li_html = '\n\t<li>{}</li>'

def get_groundtruth_info(img_id, gtruth_json):
	for g in gtruth_json:
		if img_id == g['imgid']:
			return g['filename'], g['sentences'][0]['raw'], g['sentences'][1]['raw'], g['sentences'][2]['raw']
	raise ValueError('{} not in ground truth json!'.format(img_id))

def predictions_visualizer(predictions_json_fname):
	predictions_json = json.load(open(predictions_json_fname))
	gtruth_json = json.load(open('../../scripts/convcap/data/dataset.json'))['images']

	for pred in predictions_json:
		img_id, caption = pred['image_id'], pred['caption']

		fname, gcaption1, gcaption2, gcaption3 = get_groundtruth_info(img_id, gtruth_json)

		gcaps = [gcaption1, gcaption2, gcaption3]

		html = img_html.format(fname) + '\n<p><b>{}</b></p>\n'.format(caption) + ul_html.format(''.join([li_html.format(g) for g in gcaps]))

		# Image.open('./data/test/' + fname).save()
		# print(caption)
		# print(li_html.format(gcaption1))
		# print(li_html.format(gcaption2))
		# print(li_html.format(gcaption3))

		# raw_input()

		raw_input(html)

if __name__ == '__main__':
	predictions_visualizer(sys.argv[1])