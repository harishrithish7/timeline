window.onload = function () {
	var fname = './files/dataset/descriptions.json';

	var grid_col = 2;
	var rows;

	$.getJSON(fname, function (data) {
		rows = data['data'];
		console.log('Total number of samples: ' + rows.length);

		var grid_rows = parseInt(Math.ceil(rows.length / grid_col.toFixed(1)))

		function show_data(data) {
			document.getElementById('result_summary').innerHTML = 'Showing ' + data.length + ' images';

			document.getElementById('data').innerHTML = '';

			if(data.length == 0) {
				document.getElementById('data').innerHTML = '<h3>No samples found.</h3>';
			}

			li_html = '<li>{desc}</li>'
			col_html = '<div class="data_column">\
				<img class="datapoint" src={src} style="width : 150px; height : 150px;">\
				</div>'

			col_html_for_ann = '<div class="data_column"><ol style="text-align : left;">{desc_list}</ol></div>'
			row_html = '<div class="data_row" style="text-align: center;">{col_html_filler}</div><br><br>'

			for (i = 0; i < grid_rows; i++) {
				var remaining = Math.min(grid_col, data.length - (i * grid_col));

				var cols_html = '';

				for (var j = i * grid_col; j < i * grid_col + remaining; j++) {
					cur_col_html_img = col_html.replace('{src}', '"files/dataset/faces/' + data[j]['fname'] + '"');
					cols_html += cur_col_html_img;
					cur_li_html = '';

					Object.keys(data[j]['anns']).sort().forEach(function(key) {
						cur_li_html += li_html.replace('{desc}', data[j]['anns'][key]);
					})

					cur_col_html_ann = col_html_for_ann.replace('{desc_list}', cur_li_html);
					cols_html += cur_col_html_ann;
				}

				document.getElementById('data').innerHTML += row_html.replace('{col_html_filler}', cols_html);
			}

			var elements = document.getElementsByClassName('data_column');
			for (var i = elements.length - 1; i >= 0; i--) {
				if(i % 2 == 0)
					elements[i].style.width = (100. / (grid_col * 5)) + "%";
				else
					elements[i].style.width = (100. / (grid_col * 1.4)) + "%";
			}
		}

		show_data(rows)

		if(typeof(String.prototype.strip) === "undefined")
		{
			String.prototype.strip = function() 
			{
				return String(this).replace(/^\s+|\s+$/g, '');
			};
		}

		document.getElementById("search_form").addEventListener("submit", function(){
			var query = document.getElementById("query").value.strip();

			console.log('Searching through ' + rows.length + ' rows..');

			var filtered_data = [];

			for (var i = 0; i < rows.length; i++) {
				
				var flag = false;

				Object.keys(rows[i]['anns']).sort().forEach(function(key) {
					flag = (flag || rows[i]['anns'][key].includes(query));
				})

				if(flag) filtered_data.push(rows[i]);
			}

			show_data(filtered_data);
		});

		document.getElementById("reset").addEventListener("click", function(){
			document.getElementById("query").value = '';
			show_data(rows);
		});

	});

	/*

	show_data();*/
}